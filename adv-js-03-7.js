// TASK 7:
// Доповніть код так, щоб він коректно працював

// ```javascript
// const array = ['value', () => 'showValue'];

// // Допишіть код тут:
// alert(value); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'
// ```

function task7() {
  const array = ["value", () => "showValue"];

  // // OPTION 1 >>> STRAIGHT METHOD:
  // const value = array[0];
  // const showValue = array[1];

  // OPTION 2:
  const [value, showValue] = array;

  alert(value);
  alert(showValue());
}

console.log("");
console.log("TASK 7: Shows Alert Function");
task7();
