// TASK 5:

// Дано масив книг (books). Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив):

function task5() {
  const books = [
    { name: "Harry Potter", author: "J.K. Rowling" },
    { name: "Lord of the Rings", author: "J.R.R. Tolkien" },
    { name: "The Witcher", author: "Andrzej Sapkowski" },
  ];

  const bookToAdd = { name: "Game of Thrones", author: "George R. R. Martin" };

  //   // OPTION 1 >>> OLD SCHOOL:
  //   const allBooks = books.concat(bookToAdd);
  //   console.log(allBooks);

  //   // OPTION 2:
  //   const allBooks = [...books, { ...bookToAdd }];
  //   console.log(allBooks);

    // OPTION 3 >>> SIMPLE METHOD:
    const allBooks = [...books, bookToAdd];
    console.log(allBooks);
}

console.log("");
console.log("TASK 5: ");
task5();
