// TASK 1:
// Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.
// У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив, який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.

function task1() {
  const clients1 = [ "Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет", ];
  const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

  // OPTION 1 >>> SIMPLE MERGE without set:
  let merge = [...clients1, ...clients2];

  // // OPTION 2 >>> SET-MERGE:
  // let allClients = new Set([...clients1, ...clients2]);

  // // OPTION 3 >>> SET-MERGE:
  let mergeSet = new Set(merge);

  // Old School Method:
  // let allClients = Array.from(mergeSet); 

  // New Destructuring method:
  let allClients = [...mergeSet]; 

  // // FOR COMPARISON:
  // console.log("The original client database of the first company: ", clients1);
  // console.log("The original client database of the second company: ", clients2);
  // console.log("The common merged client database: ", merge);
  
  console.log("The merge of the Unique Client Database: ", allClients);
}

console.log("TASK 1: ");
task1();