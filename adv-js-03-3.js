// TASK 3:
// У нас є об'єкт' `user`: ```{javascript}```

// Напишіть деструктуруюче присвоєння, яке:
//   - властивість name присвоїть в змінну ім'я
//   - властивість years присвоїть в змінну вік
//   - властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
 
// Виведіть змінні на екран.

function task3() {
  const user1 = { name: "John", years: 30 };

  let {name, years, isAdmin = false} = user1;
  console.log(name, years, isAdmin);
}

console.log("");
console.log("TASK 3: ");
task3();