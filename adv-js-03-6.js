// TASK 6:
// Даний об'єкт `employee`. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

function task6() {
  const employee = { name: "Vitalii", surname: "Klichko" };

//   //   OPTION 1 >>> SIMPLE METHOD:
//   const expandEmployee = { ...employee, age: 52, salary: 24000 };
//   console.log(expandEmployee);

    // OPTION 2 >>> THROUGH FUNCTION:
    function expandEmployee(existObj, age, salary) {
      const newProperty = { ...existObj, age, salary };
      console.log(newProperty);
    }
    expandEmployee(employee, 52, 24000);
}

console.log("");
console.log("TASK 6: ");
task6();
